/* -------------- */
/* --- main.c --- */
/* -------------- */

#include <stdio.h>
#include <stdlib.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"

#include "simd1D.h"
#include "simd2D.h"
#include "heat_test.h"

#include "mymacro.h"

#include <omp.h>
void info()
{
#ifdef ENABLE_BENCHMARK
    puts("mode Benchmark ON");
    puts("DEBUG OFF");
    puts("pour desactiver le mode Benchmark, mettre en commentaire le define ENABLE_BENCHMARK dans mymacro.h");
#else
    puts("mode Benchmark OFF");
    puts("DEBUG ON");
#endif

#ifdef OPENMP
    puts("mode OpenMP ON");
    omp_set_num_threads(8); // equal au nb de cores ou le double si techno HT
#else
    puts("mode OpenMP OFF");
#endif
    
}
int main(int argc, char *argv[])
{
    info();

    //main_1D(argc, argv);
    main_2D(argc, argv);

    return 0;   
}