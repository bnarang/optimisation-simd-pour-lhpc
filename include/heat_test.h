/* ------------------- */
/* --- heat_test.h --- */
/* ------------------- */

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */

#ifndef __HEAT_TEST_H__
#define __HEAT_TEST_H__

int main_heat(int argc, char * argv[]);

#endif //__HEAT_TEST_H__
