/* -------------------- */
/* --- vector_SSE.c --- */
/* -------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"

#include "mymacro.h"
#include "simd_macro.h"

#include "x86intrin.h"

#include <time.h>

// -------------------------------------------------------------------
void add_vf32vector(vfloat32 *vX1, vfloat32 *vX2, int n, vfloat32 *vY)
// -------------------------------------------------------------------
{
    int i;
    vfloat32 x1, x2, y;

    for(i=0; i<n; i++) {
        
        x1 = _mm_load_ps((float32*) &vX1[i]);
        x2 = _mm_load_ps((float32*) &vX2[i]);
        
        y = _mm_add_ps(x1, x2);
        
        _mm_store_ps((float*) &vY[i], y);
    }
}
// ---------------------------------------------------------
vfloat32 dot_vf32vector(vfloat32 *vX1, vfloat32 *vX2, int n)
// ---------------------------------------------------------
{
    int i;
    vfloat32 x1, x2, p, s, s2, s3, s4;

    s = _mm_set_ps(0,0,0,0);

    for (i=0; i<n; i++) {
        // xk = vXk[i]
        x1 = _mm_load_ps((float32*) &vX1[i]);
        x2 = _mm_load_ps((float32*) &vX2[i]);
        // p = x1*x2
        p  = _mm_mul_ps(x1, x2);
        // s = p+s
        s  = _mm_add_ps(s, p);
    }

    return VEC_ADD_AUTO(s);
}
// ----------------------------------------------------
void avg3_vf32vector(vfloat32 *vX, int n, vfloat32 *vY)
// ----------------------------------------------------
{
    int i;
    vfloat32 x0, x1, x2, cste;
    cste = _mm_set_ps(1.0/3.0, 1.0/3.0, 1.0/3.0, 1.0/3.0);

    for (i=0; i<n; i++) {
        // x1 = [4,5,6,7]
        x1 = _mm_load_ps((float32*) &vX[i]);
        // x0 = [3,4,5,6]
        x0 = VEC_LEFT(_mm_load_ps((float32*) &vX[i-1]), x1);
        // x2 = [5,6,7,8]
        x2 = VEC_RIGHT(x1, _mm_load_ps((float32*) &vX[i+1]));

        // vY[i] = (x0+x1+x2)*cste
        _mm_store_ps( (float*) &vY[i], _mm_mul_ps(VEC_ADD_3(x0, x1, x2), cste) );
    }
}
// ----------------------------------------------------
void avg5_vf32vector(vfloat32 *vX, int n, vfloat32 *vY)
// ----------------------------------------------------
{
    int i;
    vfloat32 x0, x1, x2, x3, x4, cste;
    cste = _mm_set_ps(1.0/5.0, 1.0/5.0, 1.0/5.0, 1.0/5.0);

    for (i=0; i<n; i++) {
        // x2 = [4,5,6,7]
        x2 = _mm_load_ps((float32*) &vX[i]);
        // x0 = [2,3,4,5]
        x0 = VEC_LEFT2(     _mm_load_ps((float32*) &vX[i-1]), x2);
        // x1 = [3,4,5,6]
        x1 = VEC_LEFT(      _mm_load_ps((float32*) &vX[i-1]), x2);
        // x2 = [5,6,7,8]
        x3 = VEC_RIGHT(x2,  _mm_load_ps((float32*) &vX[i+1]));
        // x2 = [6,7,8,9]
        x4 = VEC_RIGHT2(x2, _mm_load_ps((float32*) &vX[i+1]));

        // vY[i] = (x0+x1+x2+x3+x4)*cste
        _mm_store_ps( (float*) &vY[i], _mm_mul_ps(VEC_ADD_5(x0, x1, x2, x3, x4), cste) );
    }
}
// --------------------------------------------------------
void avg3_rot_vf32vector(vfloat32 *vX, int n, vfloat32 *vY)
// --------------------------------------------------------
{
    int i = 0;
    vfloat32 x0, x1, x2, cste;
    x0   = _mm_load_ps((float32*) &vX[i-1]);
    x1   = _mm_load_ps((float32*) &vX[i]);
    cste = _mm_set_ps(1.0/3.0, 1.0/3.0, 1.0/3.0, 1.0/3.0);

    for (i=0; i<n; i++) {
        x2 = _mm_load_ps((float32*) &vX[i+1]);
        // vY[i] = (x0+x1+x2)*cste
        _mm_store_ps( (float*) &vY[i], _mm_mul_ps( VEC_ADD_3( VEC_LEFT(x0, x1), x1, VEC_RIGHT(x1, x2) ), cste) );
        x0 = x1;
        x1 = x2;
    }
}
// --------------------------------------------------------
void avg5_rot_vf32vector(vfloat32 *vX, int n, vfloat32 *vY)
// --------------------------------------------------------
{
    int i = 0;
    vfloat32 x0, x1, x2, cste;
    x0   = _mm_load_ps((float32*) &vX[i-1]);
    x1   = _mm_load_ps((float32*) &vX[i]);
    cste = _mm_set_ps(1.0/5.0, 1.0/5.0, 1.0/5.0, 1.0/5.0);

    for (i=0; i<n; i++) {
        x2 = _mm_load_ps((float32*) &vX[i+1]);
        // vY[i] = (x0+x1+x2+x3+x4)*cste
        _mm_store_ps( (float*) &vY[i], _mm_mul_ps( VEC_ADD_5( VEC_LEFT2(x0, x1), VEC_LEFT(x0, x1), x1, VEC_RIGHT(x1, x2), VEC_LEFT2(x1, x2)), cste) );
        x0 = x1;
        x1 = x2;
    }
}


/* ========================== */
/* === Fonctions de tests === */
/* ========================== */


// --------------
void test1D(void)
// --------------
{
    int b = 2;
    int n;
    int card; // cardinal of vector type

    int si0, si1; // scalar indices
    int vi0, vi1; // vector indices
    int mi0, mi1; // memory (bounded) indices

    int si0b, si1b; // scalar indices with border
    int vi0b, vi1b; // vector indices with border
    int mi0b, mi1b; // memory (bounded) indices  with border

    
    vfloat32 *vX1, *vX2, *vY, *vY3, *vY5;
    vfloat32 d;

    char* format = "%6.2f ";
    
    // chronometrie
    int iter, niter = 4;
    int run, nrun = 5;
    double t0, t1, dt, tmin, t;
    double cycles;

    clock_t begin, end;
    double time_spent;

    
    puts("===============");
    puts("=== test 1D ===");
    puts("===============");

    // ------------------------- //
    // -- calculs des indices -- //
    // ------------------------- //

    DEBUG(n=8);
    BENCH(n=100);

    printf("n = %d\n", n);
    
    card = card_vfloat32();

    si0 = 0;
    si1 = n-1;
    
    s2v1D(si0, si1, card, &vi0, &vi1);
    v2m1D(vi0, vi1, card, &mi0, &mi1);
    
    si0b = si0-b;
    si1b = si1+b;

    s2v1D(si0b, si1b, card, &vi0b, &vi1b);
    v2m1D(vi0b, vi1b, card, &mi0b, &mi1b);

    // ------------------------------------------- //
    // -- allocation des tableaux 1D vectoriels -- //
    // ------------------------------------------- //

    vX1 = vf32vector(vi0b, vi1b);
    vX2 = vf32vector(vi0b, vi1b);
    
    vY  = vf32vector(vi0, vi1);
    vY3 = vf32vector(vi0, vi1);
    vY5 = vf32vector(vi0, vi1);

    // ---------- //
    // -- init -- //
    // ---------- //

    zero_vf32vector(vX1, vi0b, vi1b);
    zero_vf32vector(vX2, vi0b, vi1b);
    zero_vf32vector(vY,  vi0, vi1);
    zero_vf32vector(vY3, vi0, vi1);
    zero_vf32vector(vY5, vi0, vi1);

    init_vf32vector_param(vX1, vi0, vi1, 1, 1);
    init_vf32vector_param(vX2, vi0, vi1, 1, 2);
    
    // --------------- //
    // -- affichage -- //
    // --------------- //

    // affichage classique sur une ligne: appel de la fonction scalaire
    DEBUG(display_f32vector((float32*) vX1, si0, si1, "%4.0f", "sX1"));

    // affichage par bloc SIMD: appel de la fonction SIMD
    DEBUG(display_vf32vector(vX1, vi0, vi1, "%4.0f", "vX1"));
    DEBUG(puts(""));

    // affichage classique sur une ligne: appel de la fonction scalaire
    DEBUG(display_f32vector((float32*) vX2, si0, si1, "%4.0f", "sX2"));

    // affichage par bloc SIMD: appel de la fonction SIMD
    DEBUG(display_vf32vector(vX2, vi0, vi1, "%4.0f", "vX2"));
    DEBUG(puts(""));

    // ------------ //
    // -- calcul -- //
    // ------------ //
    
    puts("----------------");
    puts("--- addition ---");
    puts("----------------");
    
    begin = clock();
    CHRONO(add_vf32vector(vX1, vX2, n/card, vY),cycles);
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\nadd: ");
    DEBUG(display_vf32vector(vY, vi0, vi1, format, "Y")); DEBUG(puts(""));
    BENCH(printf(format, cycles/n); printf("   %f sec\n", (double)(time_spent))); BENCH(puts(""));
    
    // affichage classique sur une ligne: appel de la fonction scalaire
    DEBUG(display_f32vector((float32*) vY, si0, si1, "%4.0f", "sY"));
    DEBUG(puts(""));
    
    puts("-------------------");
    puts("--- dot product ---");
    puts("-------------------");
    
    begin = clock();
    CHRONO(d = dot_vf32vector(vX1, vX2, n/card),cycles);
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\ndot product: ");
    DEBUG(display_vfloat32(d, "%6.0f ", "\nd")); DEBUG(puts(""));
    BENCH(printf(format, cycles/n); printf("   %f sec\n", (double)(time_spent)); );
    display_vfloat32(d, "%6.0f ", "\nd"); puts(""); // laisser sinon dead-code-elimination enleve tout le calcul ...
    
    puts("-----------");
    puts("--- avg ---");
    puts("-----------");
        
    begin = clock();
    CHRONO(avg3_vf32vector(vX1, n/card, vY3),cycles); printf("avg 3   "); DEBUG(display_vf32vector(vY3, vi0, vi1, format, "Y3")); BENCH(printf(format, cycles/n)); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg5_vf32vector(vX1, n/card, vY5),cycles); printf("avg 5   "); DEBUG(display_vf32vector(vY5, vi0, vi1, format, "Y5")); BENCH(printf(format, cycles/n)); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    BENCH(puts(""));
    
    begin = clock();
    CHRONO(avg3_rot_vf32vector(vX1, n/card, vY3),cycles); printf("avg 3 rot"); DEBUG(display_vf32vector(vY3, vi0, vi1, format, "Y3")); BENCH(printf(format, cycles/n)); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg5_rot_vf32vector(vX1, n/card, vY5),cycles); printf("avg 5 rot"); DEBUG(display_vf32vector(vY5, vi0, vi1, format, "Y5")); BENCH(printf(format, cycles/n)); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    BENCH(puts(""));
    

    // ---------- //
    // -- free -- //
    // ---------- //

    free_vf32vector(vX1, vi0b, vi1b);
    free_vf32vector(vX2, vi0b, vi1b);
    
    free_vf32vector(vY,  vi0, vi1);
    free_vf32vector(vY3, vi0, vi1);
    free_vf32vector(vY5, vi0, vi1);

    
}
// ================================
int main_1D(int argc, char *argv[])
// ================================
{
    test1D();
    return 0;
}