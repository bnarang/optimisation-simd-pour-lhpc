/* ------------------- */
/* --- heat_test.c --- */
/* ------------------- */

#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"
#include "simd_macro.h"
#include "mymacro.h"

#include "heat.h"

//#include "x86intrin.h" // si compilateur Intel
#include "x86intrin.h"  // si compilateur GCC

/* =============== */
void test_bench(int n)
/* =============== */
{
    int card;
    //int n;
    char *format = "%6.3f ";
    
    int si0, si1, sj0, sj1; // scalar indices
    int vi0, vi1, vj0, vj1; // vector indices
    int mi0, mi1, mj0, mj1; // memory (bounded) indices
    
    int si0b, si1b, sj0b, sj1b; // scalar indices with border
    int vi0b, vi1b, vj0b, vj1b; // vector indices with border
    int mi0b, mi1b, mj0b, mj1b; // memory (bounded) indices  with border
    
    float32  **sX, **sY2, **sY4;
    vfloat32 **vX, **vY2, **vY4;
    
    float32 R, R2; // param order2
    float32 S, S2; // param order4
    
    // chronometrie
    int iter, niter = 4;
    int run, nrun = 5;
    double t0, t1, dt, tmin, t;
    double cycles;
      
    puts("===============");
    puts("=== test 2D ===");
    puts("===============");
 
    // ------------------------- //
    // -- calculs des indices -- //
    // ------------------------- //
    
    //DEBUG(n=4);
    //BENCH(n=100);
    //BENCH(n=1000);
    //BENCH(n=500);
    //BENCH(n=2000);
    //BENCH(n=10000);
 
    card = card_vfloat32();
    
    si0 = 0; si1 = n-1;
    sj0 = 0; sj1 = n-1;
    
    si0b = si0-2; si1b = si1+2;
    sj0b = sj0-2; sj1b = sj1+2;
    
    s2v(si0, si1, sj0, sj1, card, &vi0, &vi1, &vj0, &vj1);
    v2m(vi0, vi1, vj0, vj1, card, &mi0, &mi1, &mj0, &mj1);
    
    s2v(si0b, si1b, sj0b, sj1b, card, &vi0b, &vi1b, &vj0b, &vj1b);
    v2m(vi0b, vi1b, vj0b, vj1b, card, &mi0b, &mi1b, &mj0b, &mj1b);
        
    // allocation
    vX  = vf32matrix(vi0b, vi1b, vj0b, vj1b);
    vY2 = vf32matrix(vi0,  vi1,  vj0,  vj1);
    vY4 = vf32matrix(vi0,  vi1,  vj0,  vj1);
    
    // wrappers scalaires
    sX  = (float32**) vX;
    sY2 = (float32**) vY2;
    sY4 = (float32**) vY4;
    
    printf("n = %d\n\n", n);
    
    // ---------- //
    // -- init -- //
    // ---------- //
    
    R  = 1.2f;
    R2 = 2.3f;
    S  = 3.4f;
    S2 = 4.5f;
    
    zero_vf32matrix(vX,  vi0b, vi1b, vj0b, vj1b);
    zero_vf32matrix(vY2, vi0, vi1, vj0, vj1);
    zero_vf32matrix(vY4, vi0, vi1, vj0, vj1);
    
    init_vf32matrix_param(vX, vi0b, vi1b, vj0b, vj1b, 1,1,8);
 
    // display init data
    DEBUG(display_f32matrix(sX,  0, n-1, 0, n-1, format, "sX"));
    DEBUG(display_f32matrix(sY2, 0, n-1, 0, n-1, format, "sY2"));
    
    DEBUG(display_vf32matrix(vX,  vi0b, vi1b, vj0b, vj1b, format, "vX"));
    DEBUG(display_vf32matrix(vY2, vi0,  vi1,  vj0,  vj1,  format, "vY2"));
    DEBUG(display_vf32matrix(vY4, vi0,  vi1,  vj0,  vj1,  format, "vY4"));
    
    // ------------ //
    // -- calcul -- //
    // ------------ //
    CHRONO(heat2D_order2_scalar(sX, n, sY2, R, R2),cycles);        printf("order2_scalar       "); DEBUG(display_f32matrix(sY2, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    CHRONO(heat2D_order2_factor_scalar(sX, n, sY2, R, R2),cycles); printf("order2_factor_scalar"); DEBUG(display_f32matrix(sY2, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    //CHRONO(heat2D_order2_rot_scalar(sX, n, sY2, R, R2),cycles);    printf("order2_rot_scalar   "); DEBUG(display_f32matrix(sY2, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));    
    CHRONO(heat2D_order2_SIMD(vX, n/card, vY2, R, R2),cycles);     printf("order2_SIMD         "); DEBUG(display_f32matrix(sY2, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    BENCH(puts(""));
    
    CHRONO(heat2D_order4_scalar(sX, n, sY2, S, S2),cycles);        printf("order4_scalar       "); DEBUG(display_f32matrix(sY4, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    CHRONO(heat2D_order4_SIMD(vX, n/card, vY4, S, S2),cycles);     printf("order4_SIMD         "); DEBUG(display_f32matrix(sY4, si0, si1, sj0, sj1, format, "Y2")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    
    BENCH(puts(""));
    
    // ---------- //
    // -- free -- //
    // ---------- //
    
    free_vf32matrix(vX,  vi0b, vi1b, vj0b, vj1b);
    free_vf32matrix(vY2, vi0,  vi1,  vj0,  vj1);
    free_vf32matrix(vY4, vi0,  vi1,  vj0,  vj1);
}
// =================================
int main_heat(int argc, char * argv[])
// =================================
{
    int n;
    DEBUG(n=4);
    BENCH(n=1000);
    
    //test_bench(n);
    
    DEBUG(test_bench(4));
    BENCH(test_bench(500));
    BENCH(test_bench(600));
    BENCH(test_bench(700));
    BENCH(test_bench(800));
    BENCH(test_bench(900));
    BENCH(test_bench(1000));
    return 0;
}
