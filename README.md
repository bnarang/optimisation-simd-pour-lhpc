# Projet d'optimisation SIMD pour l'HPC
## Benjamin N. 
## Février 2014

### exe/

Dossier où est placé le programme compilé.

### include/

Contient tous les headers.

### obj/

Contient tous les *object files* produits durant la compilation.

### src/

C'est ici que sont situés les fichiers C écrits pour ce projet.
  * `heat.c`
  * `heat_test.c` : appel des fonctions 1D et 2D codées sur des exemples numériques.
  * `main.c`
  * `nrutil.c` : fonctions de gestion des registres.
	* `simd1D.c` : fonctions d'action sur les vecteurs (addition, produit scalaire, moyennes).
	* `simd2D.c` : fonctions d'action sur les matrices (moyennes).
  * `vnrutil.c` : fonctions de création des « vecteurs » SIMD.

