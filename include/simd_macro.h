/* -------------------- */
/* --- simd_macro.h --- */
/* -------------------- */

// macro pour la simplification des expressions arithmetiques et logique
// mais surtout de manipulation de registres SIMD (aka "vecteur")

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */


#ifndef __SIMD_MACRO_H__
#define __SIMD_MACRO_H__

// il faut recoder les macros
// 
#define vec_left1(v0, v1)  v1
#define vec_left2(v0, v1)  v1
#define vec_left3(v0, v1)  v1
#define vec_left4(v0, v1)  v0

#define vec_right1(v1, v2) v1
#define vec_right2(v1, v2) v1
#define vec_right3(v1, v2) v1
#define vec_right4(v1, v2) v2

// calculs
#define vec_div3(x) x
#define vec_div5(x) x

#define vec_add3(x0, x1, x2) x0
#define vec_add4(x0, x1, x2, x3) x0
#define vec_add5(x0, x1, x2, x3, x4) x0

#define vAVERAGE3(x0,x1,x2) x0
#define vAVERAGE5(x0,x1,x2,x3,x4) x0

#endif // __SIMD_MACRO_H__
