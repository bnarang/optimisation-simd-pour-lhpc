/* ---------------- */
/* --- simd1D.h --- */
/* ---------------- */

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */

#ifndef __SIMD_1D_H__
#define __SIMD_1D_H__

#ifdef __cplusplus
#pragma message ("C++")
extern "C" {
#endif

// fonction principale     
int main_1D(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif // __SIMD_1D_H__
