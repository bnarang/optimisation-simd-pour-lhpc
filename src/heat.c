/* -------------- */
/* --- heat.c --- */
/* -------------- */

// resolution de l'equation de la chaleur par stencil 3x3 (ordre 2) et 5x5 (ordre 4)

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */

#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"
#include "simd_macro.h"
#include "mymacro.h"

//#include "x86intrin.h" // si compilateur Intel
#include "x86intrin.h"  // si compilateur GCC
#include "heat.h"

//#define vec_VEC_ADD_3(a,b,c) _mm_add_ps(_mm_add_ps(a,b), c)

// ------------------------------------------------------------------------------------------------
void heat2D_order2_scalar(float32** RESTRICT X, int n, float32** RESTRICT Y, float32 R, float32 R2)
// ------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
// -------------------------------------------------------------------------------------------------------
void heat2D_order2_factor_scalar(float32** RESTRICT X, int n, float32** RESTRICT Y, float32 R, float32 R2)
// -------------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
// ----------------------------------------------------------------------------------------------------
void heat2D_order2_rot_scalar(float32** RESTRICT X, int n, float32** RESTRICT Y, float32 R, float32 R2)
// ----------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
// ------------------------------------------------------------------------------------------------
void heat2D_order2_SIMD(vfloat32** RESTRICT X, int n, vfloat32** RESTRICT Y, float32 R, float32 R2)
// ------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
// ------------------------------------------------------------------------------------------------
void heat2D_order4_scalar(float32** RESTRICT X, int n, float32** RESTRICT Y, float32 S, float32 S2)
// ------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
// ------------------------------------------------------------------------------------------------
void heat2D_order4_SIMD(vfloat32** RESTRICT X, int n, vfloat32** RESTRICT Y, float32 S, float32 S2)
// ------------------------------------------------------------------------------------------------
{
    // CODE A COMPLETER
}
