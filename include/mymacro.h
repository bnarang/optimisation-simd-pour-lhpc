/* ----------------- */
/* --- mymacro.h --- */
/* ----------------- */

// macro de chronometrie

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */

#ifndef __MY_MACRO_H__
#define __MY_MACRO_H__

// activer ou desactiver le define ci-dessous pour passer
// du mode de mise au point au mode benchmark

#define ENABLE_BENCHMARK
#define RESTRICT restrict

// décalages
// [1, 2, 3, 4] et [5, 6, 7, 8] -> [4, 5, 6, 7]
#define VEC_LEFT(A, B) _mm_shuffle_ps( _mm_shuffle_ps(B, A, _MM_SHUFFLE(3,2,1,0)), B, _MM_SHUFFLE(2,1,0,3) )
// [1, 2, 3, 4] et [5, 6, 7, 8] -> [2, 3, 4, 5]
#define VEC_RIGHT(B, C) _mm_shuffle_ps( B, _mm_shuffle_ps(B, C, _MM_SHUFFLE(3,0,3,0)), _MM_SHUFFLE(2,1,2,1) )
// [1, 2, 3, 4] et [5, 6, 7, 8] -> [3, 4, 5, 6]
#define VEC_LEFT2(A, B) _mm_shuffle_ps( A, B, _MM_SHUFFLE(1,0,3,2) )
#define VEC_RIGHT2(B, C) _mm_shuffle_ps( B, C, _MM_SHUFFLE(1,0,3,2) )

// Additions
#define VEC_ADD_3(A, B, C)       _mm_add_ps( _mm_add_ps(A, B), C )
#define VEC_ADD_4(A, B, C, D)    _mm_add_ps( VEC_ADD_3(A, B, C), D )
#define VEC_ADD_5(A, B, C, D, E) _mm_add_ps( VEC_ADD_4(A, B, C, D), E )

// ajoute toutes les composantes d'un registre :
//  = [1, 2, 3, 4] + [4, 1, 2, 3] + [3, 4, 1, 2] + [2, 4, 3, 1]
//  = [10, 10, 10, 10]
#define VEC_ADD_AUTO(A) VEC_ADD_4( A, _mm_shuffle_ps(A,A,_MM_SHUFFLE(2,1,0,3)), _mm_shuffle_ps(A,A,_MM_SHUFFLE(1,0,3,2)), _mm_shuffle_ps(A,A,_MM_SHUFFLE(0,3,2,1)) )

// -------------------------------------------
// -- ne rien ecrire en dessous de cette ligne
// -------------------------------------------


#ifdef ENABLE_BENCHMARK
#define CHRONO(X,t)  tmin = 1e38; for(run=0; run<nrun; run++) { t0 = (double) _rdtsc(); for(iter=0; iter<niter; iter++) { X; } t1 = (double) _rdtsc(); dt=t1-t0; if(dt<tmin) tmin = dt; } t = tmin / (double) niter
#define BENCH(X) X
#define DEBUG(X)
#else
#define CHRONO(X,t)  X
#define BENCH(X) 
#define DEBUG(X) X
#endif
#endif // __MY_MACRO_H__
