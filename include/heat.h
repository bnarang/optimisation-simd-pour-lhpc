/* -------------- */
/* --- heat.h --- */
/* -------------- */

/*
 * Copyright (c) 2004-2013 Lionel Lacassagne, All rights reserved
 * Institut d'Electronique Fondamentale,  Universite Paris Sud
 * Laboratoire de Recherche en Informatique, Universite Paris Sud
 */

#ifndef __HEAT_H__
#define __HEAT_H__

#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"
#include "simd_macro.h"
#include "mymacro.h"

#include "x86intrin.h" // si compilateur Intel

#define ENABLE_RESTRICT

#ifdef ENABLE_RESTRICT
#define RESTRICT restrict
#else
#define RESTRICT
#endif

void heat2D_order2_scalar       ( float32** RESTRICT X, int n,  float32** RESTRICT Y, float32 R, float32 R2);
void heat2D_order2_factor_scalar( float32** RESTRICT X, int n,  float32** RESTRICT Y, float32 R, float32 R2);
void heat2D_order2_rot_scalar   ( float32** RESTRICT X, int n,  float32** RESTRICT Y, float32 R, float32 R2);

void heat2D_order2_SIMD         (vfloat32** RESTRICT X, int n, vfloat32** RESTRICT Y, float32 R, float32 R2);

void heat2D_order4_scalar( float32** RESTRICT X, int n,  float32** RESTRICT Y, float32 S, float32 S2);
void heat2D_order4_SIMD  (vfloat32** RESTRICT X, int n, vfloat32** RESTRICT Y, float32 S, float32 S2);

#endif //__HEAT_H__
