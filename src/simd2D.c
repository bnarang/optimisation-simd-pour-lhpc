/* ---------------- */
/* --- simd2D.c --- */
/* ---------------- */

#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "def.h"
#include "nrutil.h"

#include "vdef.h"
#include "vnrutil.h"

#include "mutil.h"
#include "simd_macro.h"
#include "mymacro.h"

#include "x86intrin.h"

#include <time.h>

// --------------------------------------------------------
void avg3_reg_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             s, cste;
    cste = _mm_set_ps(1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0);

    for (i=0; i<4*n; i++) {
        for (j=0; j<n; j++) {
            // ligne i-1
            x00 = _mm_load_ps((float32*) &X[i-1][j-1]);
            x01 = _mm_load_ps((float32*) &X[i-1][j  ]);
            x02 = _mm_load_ps((float32*) &X[i-1][j+1]);
            // ligne i
            x10 = _mm_load_ps((float32*) &X[i  ][j-1]);
            x11 = _mm_load_ps((float32*) &X[i  ][j  ]);
            x12 = _mm_load_ps((float32*) &X[i  ][j+1]);
            // ligne i+1
            x20 = _mm_load_ps((float32*) &X[i+1][j-1]);
            x21 = _mm_load_ps((float32*) &X[i+1][j  ]);
            x22 = _mm_load_ps((float32*) &X[i+1][j+1]);

            // on somme sur tous les voisins
            s = VEC_ADD_3(x01, x11, x21);
            s = VEC_ADD_3(s, VEC_LEFT(x00, x01), VEC_RIGHT(x01, x02));
            s = VEC_ADD_3(s, VEC_LEFT(x10, x11), VEC_RIGHT(x11, x12));
            s = VEC_ADD_3(s, VEC_LEFT(x20, x21), VEC_RIGHT(x21, x22));

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );
        }
    }
}
// --------------------------------------------------------
void avg3_rot_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             s, cste;
    cste = _mm_set_ps(1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0);

    for (i=0; i<4*n; i++) {
        // initialisation "automatique" pour la ligne i : on fait simplement j = 0
        j = 0;
        x00 = _mm_load_ps((float32*) &X[i-1][j-1]);
        x01 = _mm_load_ps((float32*) &X[i-1][j  ]);
        x02 = _mm_load_ps((float32*) &X[i-1][j+1]);
        x10 = _mm_load_ps((float32*) &X[i  ][j-1]);
        x11 = _mm_load_ps((float32*) &X[i  ][j  ]);
        x12 = _mm_load_ps((float32*) &X[i  ][j+1]);
        x20 = _mm_load_ps((float32*) &X[i+1][j-1]);
        x21 = _mm_load_ps((float32*) &X[i+1][j  ]);
        x22 = _mm_load_ps((float32*) &X[i+1][j+1]);


        for (j=0; j<n; j++) {
            // on somme sur tous les voisins, même chose qu'avant
            s = VEC_ADD_3(x01, x11, x21);
            s = VEC_ADD_3(s, VEC_LEFT(x00, x01), VEC_RIGHT(x01, x02));
            s = VEC_ADD_3(s, VEC_LEFT(x10, x11), VEC_RIGHT(x11, x12));
            s = VEC_ADD_3(s, VEC_LEFT(x20, x21), VEC_RIGHT(x21, x22));

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );

            // on peut s'affranchir de certains loads...
            x00 = x01;
            x01 = x02;
            x10 = x11;
            x11 = x12;
            x20 = x21;
            x21 = x22;
            // ...mais pas tous
            x02 = _mm_load_ps((float32*) &X[i-1][j+2]);
            x12 = _mm_load_ps((float32*) &X[i  ][j+2]);
            x22 = _mm_load_ps((float32*) &X[i+1][j+2]);
        }
    }
}
// --------------------------------------------------------
void avg3_red_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             c0, c1, c2,
             s, cste;
    cste = _mm_set_ps(1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0);

    for (i=0; i<4*n; i++) {
        // initialisation "automatique" pour la ligne i : on fait simplement j = 0
        j = 0;
        x00 = _mm_load_ps((float32*) &X[i-1][j-1]);
        x01 = _mm_load_ps((float32*) &X[i-1][j  ]);
        x02 = _mm_load_ps((float32*) &X[i-1][j+1]);
        x10 = _mm_load_ps((float32*) &X[i  ][j-1]);
        x11 = _mm_load_ps((float32*) &X[i  ][j  ]);
        x12 = _mm_load_ps((float32*) &X[i  ][j+1]);
        x20 = _mm_load_ps((float32*) &X[i+1][j-1]);
        x21 = _mm_load_ps((float32*) &X[i+1][j  ]);
        x22 = _mm_load_ps((float32*) &X[i+1][j+1]);

        // les colonnes
        c0 = VEC_ADD_3(x00, x10, x20);
        c1 = VEC_ADD_3(x01, x11, x21);
        c2 = VEC_ADD_3(x02, x12, x22);

        for (j=0; j<n; j++) {
            // on somme sur tous les voisins, même chose qu'avant
            s = VEC_ADD_3( VEC_LEFT(c0, c1), c1, VEC_RIGHT(c1, c2) );

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );

            // on charge la suite
            c0 = c1;
            c1 = c2;

            x02 = _mm_load_ps((float32*) &X[i-1][j+2]);
            x12 = _mm_load_ps((float32*) &X[i  ][j+2]);
            x22 = _mm_load_ps((float32*) &X[i+1][j+2]);

            c2 = VEC_ADD_3(x02, x12, x22);
        }
    }
}
// --------------------------------------------------------
void avg5_reg_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             x30, x31, x32,
             x40, x41, x42,
             s, cste;
    cste = _mm_set_ps(1.0/25.0, 1.0/25.0, 1.0/25.0, 1.0/25.0);

    for (i=0; i<4*n; i++) {
        for (j=0; j<n; j++) {
            // ligne i-2
            x00 = _mm_load_ps((float32*) &X[i-2][j-1]);
            x01 = _mm_load_ps((float32*) &X[i-2][j  ]);
            x02 = _mm_load_ps((float32*) &X[i-2][j+1]);
            // // ligne i-1
            x10 = _mm_load_ps((float32*) &X[i-1][j-1]);
            x11 = _mm_load_ps((float32*) &X[i-1][j  ]);
            x12 = _mm_load_ps((float32*) &X[i-1][j+1]);
            // // ligne i
            x20 = _mm_load_ps((float32*) &X[i  ][j-1]);
            x21 = _mm_load_ps((float32*) &X[i  ][j  ]);
            x22 = _mm_load_ps((float32*) &X[i  ][j+1]);
            // // ligne i+1
            x30 = _mm_load_ps((float32*) &X[i+1][j-1]);
            x31 = _mm_load_ps((float32*) &X[i+1][j  ]);
            x32 = _mm_load_ps((float32*) &X[i+1][j+1]);
            // // ligne i+2
            x40 = _mm_load_ps((float32*) &X[i+2][j-1]);
            x41 = _mm_load_ps((float32*) &X[i+2][j  ]);
            x42 = _mm_load_ps((float32*) &X[i+2][j+1]);
            
            // on somme sur tous les voisins
            s = VEC_ADD_5(x01, x11, x21, x31, x41);
            s = VEC_ADD_5(s, VEC_LEFT2(x00, x01), VEC_LEFT(x00, x01), VEC_RIGHT(x01, x02), VEC_LEFT2(x01, x02));
            s = VEC_ADD_5(s, VEC_LEFT2(x10, x11), VEC_LEFT(x10, x11), VEC_RIGHT(x11, x12), VEC_LEFT2(x11, x12));
            s = VEC_ADD_5(s, VEC_LEFT2(x20, x21), VEC_LEFT(x20, x21), VEC_RIGHT(x21, x22), VEC_LEFT2(x21, x22));
            s = VEC_ADD_5(s, VEC_LEFT2(x30, x31), VEC_LEFT(x30, x31), VEC_RIGHT(x31, x32), VEC_LEFT2(x31, x32));
            s = VEC_ADD_5(s, VEC_LEFT2(x40, x41), VEC_LEFT(x40, x41), VEC_RIGHT(x41, x42), VEC_LEFT2(x41, x42));

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );
        }
    }
}
// --------------------------------------------------------
void avg5_rot_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             x30, x31, x32,
             x40, x41, x42,
             s, cste;
    cste = _mm_set_ps(1.0/25.0, 1.0/25.0, 1.0/25.0, 1.0/25.0);

    for (i=0; i<4*n; i++) {
        j = 0;
        x00 = _mm_load_ps((float32*) &X[i-2][j-1]);
        x01 = _mm_load_ps((float32*) &X[i-2][j  ]);
        x02 = _mm_load_ps((float32*) &X[i-2][j+1]);
        x10 = _mm_load_ps((float32*) &X[i-1][j-1]);
        x11 = _mm_load_ps((float32*) &X[i-1][j  ]);
        x12 = _mm_load_ps((float32*) &X[i-1][j+1]);
        x20 = _mm_load_ps((float32*) &X[i  ][j-1]);
        x21 = _mm_load_ps((float32*) &X[i  ][j  ]);
        x22 = _mm_load_ps((float32*) &X[i  ][j+1]);
        x30 = _mm_load_ps((float32*) &X[i+1][j-1]);
        x31 = _mm_load_ps((float32*) &X[i+1][j  ]);
        x32 = _mm_load_ps((float32*) &X[i+1][j+1]);
        x40 = _mm_load_ps((float32*) &X[i+2][j-1]);
        x41 = _mm_load_ps((float32*) &X[i+2][j  ]);
        x42 = _mm_load_ps((float32*) &X[i+2][j+1]);

        for (j=0; j<n; j++) {
            s = VEC_ADD_5(x01, x11, x21, x31, x41);
            s = VEC_ADD_5(s, VEC_LEFT2(x00, x01), VEC_LEFT(x00, x01), VEC_RIGHT(x01, x02), VEC_LEFT2(x01, x02));
            s = VEC_ADD_5(s, VEC_LEFT2(x10, x11), VEC_LEFT(x10, x11), VEC_RIGHT(x11, x12), VEC_LEFT2(x11, x12));
            s = VEC_ADD_5(s, VEC_LEFT2(x20, x21), VEC_LEFT(x20, x21), VEC_RIGHT(x21, x22), VEC_LEFT2(x21, x22));
            s = VEC_ADD_5(s, VEC_LEFT2(x30, x31), VEC_LEFT(x30, x31), VEC_RIGHT(x31, x32), VEC_LEFT2(x31, x32));
            s = VEC_ADD_5(s, VEC_LEFT2(x40, x41), VEC_LEFT(x40, x41), VEC_RIGHT(x41, x42), VEC_LEFT2(x41, x42));

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );

            x00 = x01;
            x01 = x02;
            x10 = x11;
            x11 = x12;
            x20 = x21;
            x21 = x22;
            x30 = x31;
            x31 = x32;
            x40 = x41;
            x41 = x42;

            x02 = _mm_load_ps((float32*) &X[i-2][j+2]);
            x12 = _mm_load_ps((float32*) &X[i-1][j+2]);
            x22 = _mm_load_ps((float32*) &X[i  ][j+2]);
            x32 = _mm_load_ps((float32*) &X[i+1][j+2]);
            x42 = _mm_load_ps((float32*) &X[i+2][j+2]);
        }
    }
}
// --------------------------------------------------------
void avg5_red_vf32matrix(vfloat32** X, int n, vfloat32 **Y)
// --------------------------------------------------------
{
    int i, j;
    vfloat32 x00, x01, x02,
             x10, x11, x12,
             x20, x21, x22,
             x30, x31, x32,
             x40, x41, x42,
             c0, c1, c2,
             s, cste;
    cste = _mm_set_ps(1.0/25.0, 1.0/25.0, 1.0/25.0, 1.0/25.0);

    for (i=0; i<4*n; i++) {
        j = 0;
        x00 = _mm_load_ps((float32*) &X[i-2][j-1]);
        x01 = _mm_load_ps((float32*) &X[i-2][j  ]);
        x02 = _mm_load_ps((float32*) &X[i-2][j+1]);
        x10 = _mm_load_ps((float32*) &X[i-1][j-1]);
        x11 = _mm_load_ps((float32*) &X[i-1][j  ]);
        x12 = _mm_load_ps((float32*) &X[i-1][j+1]);
        x20 = _mm_load_ps((float32*) &X[i  ][j-1]);
        x21 = _mm_load_ps((float32*) &X[i  ][j  ]);
        x22 = _mm_load_ps((float32*) &X[i  ][j+1]);
        x30 = _mm_load_ps((float32*) &X[i+1][j-1]);
        x31 = _mm_load_ps((float32*) &X[i+1][j  ]);
        x32 = _mm_load_ps((float32*) &X[i+1][j+1]);
        x40 = _mm_load_ps((float32*) &X[i+2][j-1]);
        x41 = _mm_load_ps((float32*) &X[i+2][j  ]);
        x42 = _mm_load_ps((float32*) &X[i+2][j+1]);

        c0 = VEC_ADD_5(x00, x10, x20, x30, x40);
        c1 = VEC_ADD_5(x01, x11, x21, x31, x41);
        c2 = VEC_ADD_5(x02, x12, x22, x32, x42);

        for (j=0; j<n; j++) {
            s = VEC_ADD_5( VEC_LEFT2(c0, c1), VEC_LEFT(c0, c1), c1, VEC_RIGHT(c1, c2), VEC_RIGHT2(c1, c2) );

            // on multiplie par la constante, et on écrit dans Y
            _mm_store_ps( (float*) &Y[i][j], _mm_mul_ps(s, cste) );

            c0 = c1;
            c1 = c2;

            x02 = _mm_load_ps((float32*) &X[i-2][j+2]);
            x12 = _mm_load_ps((float32*) &X[i-1][j+2]);
            x22 = _mm_load_ps((float32*) &X[i  ][j+2]);
            x32 = _mm_load_ps((float32*) &X[i+1][j+2]);
            x42 = _mm_load_ps((float32*) &X[i+2][j+2]);

            c2 = VEC_ADD_5(x02, x12, x22, x32, x42);
        }
    }
}
/* =========== */
void test2D(void)
/* =========== */
{
    int card;
    int n;
    char *format = "%6.2f ";
    
    int si0, si1, sj0, sj1; // scalar indices
    int vi0, vi1, vj0, vj1; // vector indices
    int mi0, mi1, mj0, mj1; // memory (bounded) indices
    
    int si0b, si1b, sj0b, sj1b; // scalar indices with border
    int vi0b, vi1b, vj0b, vj1b; // vector indices with border
    int mi0b, mi1b, mj0b, mj1b; // memory (bounded) indices  with border
    
    float32  **sX, **sY3, **sY5;
    vfloat32 **vX, **vY3, **vY5;
    
    // chronometrie
    int iter, niter = 4;
    int run, nrun = 5;
    double t0, t1, dt, tmin, t;
    double cycles;
      

    clock_t begin, end;
    double time_spent;


    puts("===============");
    puts("=== test 2D ===");
    puts("===============");
 
    // ------------------------- //
    // -- calculs des indices -- //
    // ------------------------- //
    
    //DEBUG(n=4);
    BENCH(n=5000);
 
    card = card = card_vfloat32();
    
    si0 = 0; si1 = n-1;
    sj0 = 0; sj1 = n-1;
    
    si0b = si0-2; si1b = si1+2;
    sj0b = sj0-2; sj1b = sj1+2;
    
    s2v(si0, si1, sj0, sj1, card, &vi0, &vi1, &vj0, &vj1);
    v2m(vi0, vi1, vj0, vj1, card, &mi0, &mi1, &mj0, &mj1);
    
    s2v(si0b, si1b, sj0b, sj1b, card, &vi0b, &vi1b, &vj0b, &vj1b);
    v2m(vi0b, vi1b, vj0b, vj1b, card, &mi0b, &mi1b, &mj0b, &mj1b);
        
    // allocation
    vX  = vf32matrix(vi0b, vi1b, vj0b, vj1b);
    vY3 = vf32matrix(vi0,  vi1,  vj0,  vj1);
    vY5 = vf32matrix(vi0,  vi1,  vj0,  vj1);
    
    // wrappers scalaires
    sX  = (float32**) vX;
    sY3 = (float32**) vY3;
    sY5 = (float32**) vY5;
    
    printf("n = %d\n\n", n);
    
    // ---------- //
    // -- init -- //
    // ---------- //
    
    zero_vf32matrix(vX,  vi0b, vi1b, vj0b, vj1b);
    zero_vf32matrix(vY3, vi0, vi1, vj0, vj1);
    zero_vf32matrix(vY5, vi0, vi1, vj0, vj1);
    
    init_vf32matrix_param(vX, vi0b, vi1b, vj0b, vj1b, 1,1,8);
 
    // display init data
    DEBUG(display_f32matrix(sX,  0, n-1, 0, n-1, format, "sX"));
    DEBUG(display_f32matrix(sY3, 0, n-1, 0, n-1, format, "sY3"));
    
    DEBUG(display_vf32matrix(vX,  vi0b, vi1b, vj0b, vj1b, format, "vX"));
    DEBUG(display_vf32matrix(vY3, vi0,  vi1,  vj0,  vj1,  format, "vY3"));
    DEBUG(display_vf32matrix(vY3, vi0,  vi1,  vj0,  vj1,  format, "vY5"));
    
    // ------------ //
    // -- calcul -- //
    // ------------ //

    begin = clock();
    CHRONO(avg3_reg_vf32matrix(vX, n/card, vY3),cycles); printf("avg 3x3 reg  "); DEBUG(display_vf32matrix(vY3, vi0, vi1, vj0, vj1, format, "Y3")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg3_rot_vf32matrix(vX, n/card, vY3),cycles); printf("avg 3x3 rot  "); DEBUG(display_vf32matrix(vY3, vi0, vi1, vj0, vj1, format, "Y3")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg3_red_vf32matrix(vX, n/card, vY3),cycles); printf("avg 3x3 red  "); DEBUG(display_vf32matrix(vY3, vi0, vi1, vj0, vj1, format, "Y3")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    BENCH(puts(""));
    
    begin = clock();
    CHRONO(avg5_reg_vf32matrix(vX, n/card, vY5),cycles); printf("avg 5x5 reg  "); DEBUG(display_vf32matrix(vY5, vi0, vi1, vj0, vj1, format, "Y5")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg5_rot_vf32matrix(vX, n/card, vY5),cycles); printf("avg 5x5 rot  "); DEBUG(display_vf32matrix(vY5, vi0, vi1, vj0, vj1, format, "Y5")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    begin = clock();
    CHRONO(avg5_red_vf32matrix(vX, n/card, vY5),cycles); printf("avg 5x5 red  "); DEBUG(display_vf32matrix(vY5, vi0, vi1, vj0, vj1, format, "Y5")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("   %f sec\n", (double)(time_spent));
    //CHRONO(avg5_vf32matrix(vX, n/card, vY5),cycles); printf("avg 5x5   "); DEBUG(display_vf32matrix(vY5, vi0, vi1, format, "Y5")); BENCH(printf(format, cycles/(n*n))); BENCH(puts(""));
    BENCH(puts(""));
    
    // ---------- //
    // -- free -- //
    // ---------- //
    
    free_vf32matrix(vX, vi0b, vi1b, vj0b, vj1b);
    free_vf32matrix(vY3, vi0,  vi1,  vj0,  vj1);
    free_vf32matrix(vY5, vi0,  vi1,  vj0,  vj1);
}
// =================================
int main_2D(int argc, char * argv[])
// =================================
{
    test2D();
    return 0;
}
